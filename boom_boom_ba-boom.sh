#!/usr/bin/env sh

# Originally written to run as a cron job, which would randomly but repeatedly
# blurt out "boom boom ba-boom." It was a fun gag to deploy on the computer of
# anyone who left their computer unlocked and unsupervised.

# It turns out that sleeping a random number of seconds isn't quite so easy
# using pure bash-isms. This is the best analog I've found for doing so
python -c 'import random; import time; time.sleep(random.random() * 300)'
# Make sure the unsuspecting victim won't just mute their computer to disable
# it.
osascript -e 'set volume without output muted output volume 50'
# Time spent digging through the phonemic syntax of the say command: worth it.
say "[[inpt PHON]]bUWm bUWm bUX1bUWm"
